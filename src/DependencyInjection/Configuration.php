<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types = 1);

namespace Omni\Sylius\ShippingPlugin\DependencyInjection;

use Omni\Sylius\ShippingPlugin\Entity\ShipperConfig;
use Omni\Sylius\ShippingPlugin\Model\ShipperConfigInterface;
use Sylius\Bundle\ResourceBundle\Controller\ResourceController;
use Sylius\Bundle\ResourceBundle\Doctrine\ORM\EntityRepository;
use Sylius\Bundle\ResourceBundle\SyliusResourceBundle;
use Sylius\Component\Resource\Factory\Factory;
use Sylius\Component\Resource\Repository\RepositoryInterface;
use Symfony\Component\Config\Definition\Builder\ArrayNodeDefinition;
use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder(): TreeBuilder
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('omni_sylius_shipping');

        $parcelMachineCodes = $rootNode->children()->arrayNode('dpd')->scalarPrototype()->end()->end()->end();

        $rootNode
            ->children()
                ->scalarNode('driver')->defaultValue(SyliusResourceBundle::DRIVER_DOCTRINE_ORM)->end()
                ->arrayNode('states')
                    ->children()
                        ->arrayNode('courier_ready')
                            ->children()
                                ->arrayNode('omniva')->scalarPrototype()->end()->end()
                                ->arrayNode('dpd')->scalarPrototype()->end()->end()
                                ->arrayNode('venipak')->scalarPrototype()->end()->end()
                            ->end()
                        ->end()
                        ->arrayNode('in_transit')
                            ->children()
                                ->arrayNode('omniva')->scalarPrototype()->end()->end()
                                ->arrayNode('dpd')->scalarPrototype()->end()->end()
                                ->arrayNode('venipak')->scalarPrototype()->end()->end()
                            ->end()
                        ->end()
                        ->arrayNode('cant_deliver')
                            ->children()
                                ->arrayNode('omniva')->scalarPrototype()->end()->end()
                                ->arrayNode('dpd')->scalarPrototype()->end()->end()
                                ->arrayNode('venipak')->scalarPrototype()->end()->end()
                            ->end()
                        ->end()
                        ->arrayNode('back_to_seller')
                            ->children()
                                ->arrayNode('omniva')->scalarPrototype()->end()->end()
                                ->arrayNode('dpd')->scalarPrototype()->end()->end()
                                ->arrayNode('venipak')->scalarPrototype()->end()->end()
                            ->end()
                        ->end()
                        ->arrayNode('shipped')
                            ->children()
                                ->arrayNode('omniva')->scalarPrototype()->end()->end()
                                ->arrayNode('dpd')->scalarPrototype()->end()->end()
                                ->arrayNode('venipak')->scalarPrototype()->end()->end()
                            ->end()
                        ->end()
                        ->arrayNode('cancelled')
                            ->children()
                                ->arrayNode('omniva')->scalarPrototype()->end()->end()
                                ->arrayNode('dpd')->scalarPrototype()->end()->end()
                                ->arrayNode('venipak')->scalarPrototype()->end()->end()
                            ->end()
                        ->end()
                    ->end()
                ->end()
            ->end()
        ;

        $this->addResourcesSection($rootNode);

        return $treeBuilder;
    }


    private function addResourcesSection(ArrayNodeDefinition $node)
    {
        $node
            ->children()
                ->arrayNode('resources')
                    ->addDefaultsIfNotSet()
                        ->children()
                            ->arrayNode('shipper_config')
                                ->addDefaultsIfNotSet()
                                    ->children()
                                        ->variableNode('options')->end()
                                        ->arrayNode('classes')
                                            ->addDefaultsIfNotSet()
                                            ->children()
                                                ->scalarNode('model')->defaultValue(ShipperConfig::class)->cannotBeEmpty()
            ->end()
                                                ->scalarNode('interface')->defaultValue(ShipperConfigInterface::class)
            ->cannotBeEmpty()->end()
                                                ->scalarNode('controller')->defaultValue(ResourceController::class)
            ->cannotBeEmpty()->end()
                                                ->scalarNode('repository')->defaultValue(EntityRepository::class)
            ->cannotBeEmpty()->end()
                                                ->scalarNode('factory')->defaultValue(Factory::class)->end()
                                                ->scalarNode('form')->cannotBeEmpty()->end()
                                        ->end()
                                    ->end()
                                ->end()
                            ->end()
                        ->end()
                    ->end()
                ->end();
    }
}
